#include <iostream>
#include <string>
#include <sstream>

#include "minimal_fighter.h"

using namespace std;

template <typename T>
string to_string(T value)
{
    std::ostringstream os ;
    os << value ;
    return os.str() ;
}

string infoLog(MinimalFighter *_fighter) //right나 left의 정보를 알려주는 함수.
{
    if(_fighter->status() == Dead) return "DEAD"; 
    string output = ""; //output 이라는 출력될 string을 선언.
    output += "H" + to_string(_fighter->hp());  //H가 몇인지 나타낼 꺼임 H3같은 식으로
    output += ", ";
    output += "P" + to_string(_fighter->power()); //P가 몇인지 나타낼 꺼임 P2같은 식으로.
    
    return output;
}

bool processFight()
{
    int lHp, lPower, rHp, rPower; //왼쪽 애의 Hp와 Power: lHp lPower | 오른쪽 애의 Hp 와 Power: rHp rPower
    char command; // Hit Attack Fight 를 결정하는 문자. 'H' 'A' 'F' 로 표현.
    cin >> lHp >> lPower >> command >> rHp >> rPower; // 체력 공격력 문자 체력 공격력 순으로 입력. 
    if(cin.fail()) return false; //입력 뭐가 잘못 되면 false를 반환해서 main 함수에서 아무것도 안하게 함.
    
    MinimalFighter *left = new MinimalFighter(lHp, lPower); //left라는 애의 HP와 Power가 저장될꺼임.
    MinimalFighter *right = new MinimalFighter(rHp, rPower); //right라는 애의 HP와 Power가 저장될꺼임.
    
    if(command == 'H') left->hit(right);  //Command가 H일 때 left가 right에 hit 한다.
    else if(command == 'A') left->attack(right); //Command가 H일 때 left가 right에 attack 한다.
    else if(command == 'F') left->fight(right); //Command가 H일 때 left가 right에 fight 한다.
    
    cout << infoLog(left) << " / " << infoLog(right) << endl; //left의 정보 / right의 정보
    
    delete left; 
    delete right;
    
    return true; // 이상한거 입력 안되면 true 나오게 되어있음.
}

int main(void)
{
    while(processFight()){ /* Nothing else to do. */ }
    return 0;
}

