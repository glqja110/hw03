#include <iostream>
#include <stdlib.h>
#include "binary_search.h"

using namespace std;

BinarySearch::BinarySearch()
{
	mArrayCount=0;
}

BinarySearch::~BinarySearch()
{
	delete[] mArray;
}

void BinarySearch::sortArray()
{
	int i,j,tmp;

	for(i=0;i<mArrayCount-1;i++)
	{
		for(j=0;j<mArrayCount-1;j++)
		{
			if(mArray[j]>mArray[j+1])
			{
				tmp=mArray[j];
				mArray[j]=mArray[j+1];
				mArray[j+1]=tmp;
			}
		}
	}
}

BinarySearch::BinarySearch(int *_array, int _arrayCount)
{
	mArrayCount=_arrayCount;
	mArray= new int[mArrayCount];
	int i;
	for(i=0;i<mArrayCount;i++)
		mArray[i]=_array[i];
	sortArray();
}

int BinarySearch::getIndex(int _element)
{
	int i=0;
	int j=mArrayCount-1;
	int number=0;
	int check=0;
	while(i<j)
	{
		number=((i+j)/2);
		if(_element==mArray[number])
		{
			check+=1;
			break;
		}
	        else if(_element>mArray[number])
		{
			if(_element==mArray[mArrayCount-1])
			{
				return mArrayCount-1;
			}
			i=number+1;
		}
		else if(_element<mArray[number])
			j=number;
	}
	if(check==1)
		return number;
	else if(check==0)
		return -1;
}
	
