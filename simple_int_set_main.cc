#include <iostream>
#include <stdlib.h>
#include "simple_int_set.h"

using namespace std;

enum EOperateType
{
    Invalid = -1,
    
    Add,  //더하기
    Sub,  //빼기
    Mul,  //곱하기
    Print,  
};
typedef enum EOperateType OperateType;   //'enum EOperateType'은 이제 'OperateType'으로 칠꺼임.

int main(void)
{
    while(true)
    {
        string inputs;  //inputs 라는 string에 이제 입력할꺼임.
        getline(cin, inputs);  //inputs에 입력.
        
        if(inputs == "0") break;  //inputs가 0이면 종료.
        
        string *argv = new string[inputs.length()]; // more than count  argv라는 string 집합을 만듦. 
        string token; 
        size_t pos;  
        int argc = 0;  //argc는 입력한 것들 중에서 괄호, 숫자, 연산자 들의 갯수만 나타내는 변수가 될 거임.
        
        while(true)  //argv에 문자들 넣는 과정
        {
            pos = inputs.find(' ');
            token = inputs.substr(0, pos);
            
            argv[argc++] = token; //argv에 입력된 것이 하나씩 저장됨. 빈 공간은 없음.
            
            if(pos >= inputs.length()) break;
            else inputs.erase(0, pos + 1);
        }
        
	//다 초기화 하는 과정
        SimpleIntSet *left = NULL;  //left라는 애를 만듦
        SimpleIntSet *right = NULL; //right라는 애를 만듦.
        
        int *elements = NULL;
        int elementCount = 0;
        OperateType type = Invalid;  //type은 연산자 상태를 알려줄꺼임.
        
        for(int i=0; i<argc; ++i)
        {
            string arg = argv[i]; //string arg는 argv의 원소 하나씩을 가리킬 것임.
            if(arg == "{" && elements == NULL) elements = new int[argc]; //int *elements = new int[argc] 랑 같은 의미. 이제 처음 시작 되면 argc길이의 elements라는 배열을 만들겠다는 의미.
            else if(isdigit(arg[arg.length() - 1])) elements[elementCount++] = atoi(arg.c_str()); //가리키는 부분이 이제 숫자면 elements에 넣겠다는 소리. atoi:문자열을 정수로 변환

    
            else if(arg == "}")  //숫자 넣는 과정 다 끝나면
            {
                SimpleIntSet *newSet = new SimpleIntSet(elements, elementCount); //newSet이라는 애 만듦.
                if(left == NULL) left = newSet;  //왼쪽이 비어있으면 왼쪽을 { 숫자 } 가 채울꺼고
                else if(right == NULL) right = newSet;  //왼쪽 채워진 상태면 오른쪽을 { 숫자 } 가 채울것이다. 
                
                delete elements; //elements 이제 비우기.
                elements = NULL;
                elementCount = 0;

                if(type == Invalid) type = Print; // ???
            }
            else if(arg == "+") type = Add;
            else if(arg == "-") type = Sub;
            else if(arg == "*") type = Mul;
        }
        
        switch(type)
        {
            case Add:
                left->unionSet(*right); break; 
            case Sub:
                left->differenceSet(*right); break;
            case Mul:
                left->intersectSet(*right); break;
            default: break;
        }
        if(type != Invalid) left->printSet();
        
        delete left;
        delete right;
    }
    return 0;
}

