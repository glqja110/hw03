// simple_int_set.h
// Implement your simple_int_set.cc

#ifndef __hw03__simple_int_set__
#define __hw03__simple_int_set__

#define MAX_SIZE 1024  //집합의 크기는 이 MAX_SIZE를 넘기지 못한다.

class SimpleIntSet
{
private: 
    int *mElements;
    int mElementCount;

    void sortElements(); // you can reuse your previous sort assignment
    SimpleIntSet();
    
public:  //모든 public 함수 구현해야함.
    SimpleIntSet(int *_elements, int _count); //생성자 
    ~SimpleIntSet(); //소멸자
    
    int *elements() const; // return sorted array 정렬된 배열을 리턴
    int elementCount() const; 
    
    SimpleIntSet *unionSet(SimpleIntSet& _operand);
    SimpleIntSet *differenceSet(SimpleIntSet& _operand);
    SimpleIntSet *intersectSet(SimpleIntSet& _operand);
    
    void printSet();
};

#endif

