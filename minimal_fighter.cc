#include <iostream>
#include <string>
#include <sstream>

#include "minimal_fighter.h"

using namespace std;

int MinimalFighter::hp() const
{
	return mHp;	
}
int MinimalFighter::power() const
{
	return mPower;
}

FighterStatus MinimalFighter::status() const
{
	if(mHp>0)  //hp가 0보다 크면 Alive 상태
	{
		return Alive;
	}
	else  //아니면 Dead 상태
	{
		return Dead;
	}
}


MinimalFighter::MinimalFighter() //생성자, 체력 0, 공격력 0, 상태 Invalid로 초기화
{
	mHp=0;  
	mPower=0;
	mStatus=Invalid;
}

MinimalFighter::MinimalFighter(int _hp, int _power) // 체력과 공격력을 주어진 값으로
{
	mHp=_hp; 
	mPower=_power;  
}

void MinimalFighter::setHp(int _hp)
{
	mHp=_hp;
}

void MinimalFighter::hit(MinimalFighter *_enemy)
{
	if((_enemy->mHp)!=0&&mHp!=0)
	{
	_enemy->setHp((_enemy->mHp)-mPower);
	setHp(mHp-(_enemy->mPower));
	}
	
}

void MinimalFighter::attack(MinimalFighter *_target)
{
	_target->setHp((_target->mHp)-mPower);
	mPower=0;
}
void MinimalFighter::fight(MinimalFighter *_enemy)
{
	while(mHp>0 && _enemy->mHp>0)
	{
		_enemy->setHp((_enemy->mHp)-mPower);
		setHp(mHp-(_enemy->mPower));
	}
}


