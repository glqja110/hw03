#include <iostream>
#include <stdlib.h>

#include "simple_int_set.h"

using namespace std;

int* SimpleIntSet::elements() const
{
	return mElements;
}

int SimpleIntSet::elementCount() const
{
	return mElementCount;
}

SimpleIntSet::SimpleIntSet()
{
	mElementCount=0;
}

void SimpleIntSet::sortElements()
{
	int i,j,tmp;
	
	for(i=0;i<mElementCount-1;i++)
	{
		for(j=0;j<mElementCount-1;j++)
		{
			if(mElements[j]>mElements[j+1])
			{
				tmp=mElements[j];
				mElements[j]=mElements[j+1];
				mElements[j+1]=tmp;
			}
		}
	}
}

SimpleIntSet::~SimpleIntSet()  //소멸자
{
	delete[] mElements;
}

SimpleIntSet::SimpleIntSet(int *_elements, int _count) //생성자
{
	mElementCount=_count;
	mElements=new int[MAX_SIZE];    //mElements 를 최대 사이즈인 MAX_SIZE의 길이로 설정.
	int i,j,tmp;
	for(i=0;i<mElementCount;i++)
		mElements[i]=_elements[i];		

	//mElements 중에 같은 숫자 중복 되는거 삭제할꺼임.
	int count=0; 
	for(i=0;i<(mElementCount-1);i++)
	{
		for(j=i+1;j<mElementCount;j++)
		{
			if(mElements[i]==mElements[j])
			{
				mElements[j]=-1000;  //-1000으로 삭제되었다는 지점을 표현.
				count+=1;
			}
		}
	}

	//공백을 뒤로 밀어버리기.
	for(i=0;i<mElementCount-1;i++)
	{
		for(j=0;j<mElementCount-1;j++)
		{
			if(mElements[j]==-1000)
			{
				tmp=mElements[j];
				mElements[j]=mElements[j+1];
				mElements[j+1]=tmp;
			}
		}
	}
	mElementCount-=count;	
	sortElements();
}

SimpleIntSet* SimpleIntSet::unionSet(SimpleIntSet& _operand)
{
	//right 숫자 중에서 left 숫자랑 같은게 아무것도 없으면 left의 새로운 원소가 되는 과정.
	int count=0; //right 숫자 중에서 left 숫자랑 같은게 아무것도 없는 숫자들의 갯수.
	int check=0; //right 숫자 중에서 left 숫자랑 같은게 아무것도 없는지 체크하는 변수. 
	int i,j;
	for(i=0; i<(_operand.mElementCount) ;i++)
	{
		check=0;
		for(j=0; j<mElementCount;j++)
		{
			if((_operand.mElements[i])==mElements[j])
				check+=1;
		}
		if(check==0)
		{
			count+=1;
			mElements[mElementCount-1+count]=(_operand.mElements[i]);
		}
	}
	mElementCount+=count;  //추가한 원소만큼 mElementCount를 추가시킴.
	sortElements();
}

SimpleIntSet* SimpleIntSet::differenceSet(SimpleIntSet& _operand)
{
	int count=0; //같은 숫자들 세기	
	int i,j,tmp;
	for(i=0; i<mElementCount ;i++)
	{
		for(j=0; j<(_operand.mElementCount);j++)
		{
			if(mElements[i]==(_operand.mElements[j]))
			{
				mElements[i]=-1000;
				count+=1;
			}
		}
	}
	
	//left에서 공백으로 만들어 버린 것들 다 뒤로 밀어내는 과정.
	for(i=0;i<mElementCount-1;i++)
	{
		for(j=0;j<mElementCount-1;j++)
		{
			if(mElements[j]==-1000)
			{
				tmp=mElements[j];
				mElements[j]=mElements[j+1];
				mElements[j+1]=tmp;
			}
		}
	}
	mElementCount-=count; //숫자들이 공백으로 된 만큼 숫자를 줄임. 
	sortElements();
		
}

SimpleIntSet* SimpleIntSet::intersectSet(SimpleIntSet& _operand)
{
	int count=0; //다른 숫자들 세기	
	int i,j,tmp;
	int check=0;;
	for(i=0; i<mElementCount ;i++)
	{
		check=0;
		for(j=0; j<(_operand.mElementCount);j++)
		{
			if(mElements[i]!=(_operand.mElements[j]))
			{
				check+=1;
			}
				
		}
		if(check==(_operand.mElementCount))
		{
			mElements[i]=-1000;
			count+=1;
		}
	}
	
	//left에서 공백으로 만들어 버린 것들 다 뒤로 밀어내는 과정.
	for(i=0;i<mElementCount-1;i++)
	{
		for(j=0;j<mElementCount-1;j++)
		{
			if(mElements[j]==-1000)
			{
				tmp=mElements[j];
				mElements[j]=mElements[j+1];
				mElements[j+1]=tmp;
			}
		}
	}
	mElementCount-=count; //숫자들이 공백으로 된 만큼 숫자를 줄임. 
	sortElements();
}

void SimpleIntSet::printSet()
{
	int i;
	cout<<"{ ";
	for(i=0; i<mElementCount; i++)
	{
		cout<<mElements[i]<<' ';
	}
	cout<<"}"<<endl;
}

