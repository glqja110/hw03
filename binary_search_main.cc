#include <iostream>
#include <stdlib.h>
#include "binary_search.h"

using namespace std;

int main(void)
{
    BinarySearch *binarySearch = NULL; //binarySearch 라는 이름의 Class 선언한것 아무것도 안들어 있음.
    
    while(true)
    {
        if(binarySearch == NULL) // input array
        {
            string inputs;
            getline(cin, inputs);
            
            string *argv = new string[inputs.length()]; // more than count
            string token;
            size_t pos;
            int argc = 0;
            
            while(true)  //argv에 { 숫자 } 넣는 것.
            {
                pos = inputs.find(' ');
                token = inputs.substr(0, pos);
                
                argv[argc++] = token;
                
                if(pos >= inputs.length()) break;
                else inputs.erase(0, pos + 1);
            }
            
            int *elements = NULL;
            int elementCount = 0;
            
            for(int i=0; i<argc; ++i)
            {
                string arg = argv[i];
                if(arg == "{" && elements == NULL) elements = new int[argc];
                else if(isdigit(arg[arg.length() - 1])) elements[elementCount++] = atoi(arg.c_str());   //elements 배열에 숫자 넣는것.
                else if(arg == "}")
                {
                    binarySearch = new BinarySearch(elements, elementCount);
                    
                    delete elements;
                    elements = NULL;
                    elementCount = 0;
                }
            }
        }
        else // input element to find
        {
            int element;
            cin >> element;  //숫자 입력
            
            if(element < -998) break;
            
            cout << binarySearch->getIndex(element) << endl;
        }
    }
    
    delete binarySearch;
    return 0;
}

