#include <iostream>
#include <string>
#include "reply_admin.h"

using namespace std;

ReplyAdmin::ReplyAdmin()
{
	chats=new string[NUM_OF_CHAT];
	addChat("Hello, Reply Administrator!");
	addChat("I will be a good programmer.");
 	addChat("This class is awesome.");
 	addChat("Professor Lim is wise.");
 	addChat("Two TAs are kind and helpful.");
 	addChat("I think male TA looks cool.");
}

ReplyAdmin::~ReplyAdmin()
{
	delete[] chats;
}

int ReplyAdmin::getChatCount()
{
	int i;
	for(i=0; i<NUM_OF_CHAT; ++i)
	{
		string s = chats[i];
		if(s.empty() == true) break;
	}
	return i;
}

bool ReplyAdmin::addChat(string _chat)
{
	chats[getChatCount()]=_chat;
}

bool ReplyAdmin::removeChat(int _index)
{
	int k;
	//chats 배열 개수 세기.
	for(k=0; k<NUM_OF_CHAT; ++k)
	{
		string s = chats[k];
		if(s.empty() == true) break;
	}
	if(_index>=getChatCount())
		return false;

	chats[_index]="";

	//빈공간 밀어내기 
	int i,j;
	string tmp;
	for(i=0;i<k-1;i++)
	{
		for(j=0;j<k-1;j++)
		{
			if(chats[j]=="")
			{
				tmp=chats[j];
				chats[j]=chats[j+1];
				chats[j+1]=tmp;
			}
		}
	}
}

bool ReplyAdmin::removeChat(int *_indices, int _count)
{
	int i,j,k,number;
	//배열 개수 세기.
	for(k=0; k<NUM_OF_CHAT; ++k)
	{
		string s = chats[k];
		if(s.empty() == true) break;
	}
	
	for(i=0;i<_count;i++)
	{
		number=_indices[i];
		chats[number]="";
	}

	string tmp;
	for(i=0;i<k-1;i++)
	{
		for(j=0;j<k-1;j++)
		{
			if(chats[j]=="")
			{
				tmp=chats[j];
				chats[j]=chats[j+1];
				chats[j+1]=tmp;
			}
		}
	}

}

bool ReplyAdmin::removeChat(int _start, int _end)
{
	int i,j,k,number;
	//배열 개수 세기
	for(k=0; k<NUM_OF_CHAT; ++k)
	{
		string s = chats[k];
		if(s.empty() == true) break;
	}

	for(i=_start;i<=_end;i++)
	{
		chats[i]="";
	}

	string tmp;
	for(i=0;i<k-1;i++)
	{
		for(j=0;j<k-1;j++)
		{
			if(chats[j]=="")
			{
				tmp=chats[j];
				chats[j]=chats[j+1];
				chats[j+1]=tmp;
			}
		}
	}
	
}
void ReplyAdmin::printChat()
{
	int count = getChatCount();
	for(int i=0; i<count; ++i)
	{
		cout << i << " " << chats[i] <<endl;
	}
}
